<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div class="error-404-content">
    <div class="container">
        <div class="row inner">
            <div class="col-xs-18-18 col-offset-sm-0-18 col-sm-10-18 col-offset-md-0-18 col-md-10-18 col-offset-lg-1-18 col-lg-9-18 col-offset-xl-1-18 col-xl-7-18 wrap-text">
                <p class="caption">404</p>
                <p class="subcaption"><?php _e('Page not found!'); ?></p>
                <p><?php _e('We are sorry, the page you are looking for is no longer available. <br/>
                Use the button below to go to the home page'); ?></p>
                <a class="btn-primary" href="<?php echo get_site_url(); ?>"><?php _e('go home'); ?></a>
            </div>
            
            <div class="col-xs-18-18 col-offset-sm-0-18 col-sm-8-18 col-offset-md-0-18 col-md-8-18 col-offset-lg-1-18 col-lg-6-18 col-offset-xl-0-18 col-xl-6-18 wrap-icon">
                <img src="<?php echo get_template_directory_uri() . '/build/images/maze.svg'; ?>" alt="maze" />
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
