
        <?php do_action('corppix_before_site_footer'); ?>

        <footer id="site-footer" class="site-footer"></footer>

        <?php do_action('corppix_after_site_footer'); ?>

    </main><!-- end of <main> -->
</div><!-- .wrapper -->


<?php
wp_footer();
do_action('corppix_before_body_closing_tag');
?>
</body>
</html>
