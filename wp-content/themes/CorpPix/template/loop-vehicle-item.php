<div class="vehicle-item" data-id="<?php echo $post_ID; ?>">
    
    <?php if ( !empty($img_url) ) { ?>
        <figure class="featured-thumbnail">
            <a href="<?php echo get_permalink($post_ID); ?>">
                <img src="<?php echo $img_url; ?>" alt="thumb" />
            </a>
        </figure>
    <?php } ?>
    
    <div class="wrap-info">
        <p class="title"><?php echo do_shortcode($post_title); ?></p>
        <div class="meta">
            <p class="year"><strong><?php _e('Year:'); ?> </strong><?php echo $year; ?></p>
            
            <?php
            if ( !empty($manufacturer_list) ) {
                echo '<p class="manufacturer">';
                    echo '<strong>'.__("Manufacturer:").' </strong>';
                    foreach ($manufacturer_list as $manufacturer) {
                        echo $manufacturer->name;
                    }
                echo '</p>';
            }
            
            
            if ( !empty($class_list) ) {
                echo '<p class="manufacturer">';
                    echo '<strong>'.__("Class:").' </strong>';
                    foreach ($class_list as $class) {
                        echo $class->name;
                    }
                echo '</p>';
            }
            ?>
        </div>
        <div class="price"><span class="currency">$</span><?php echo $price; ?></div>
    </div>
    
</div>