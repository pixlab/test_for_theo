<?php get_header(); ?>
    
<div class="page-content">
    <?php
    if ( have_posts() ) :
        while ( have_posts() ) : the_post();
            the_content();
        endwhile;
    endif;
    
    do_action('corppix_after_page_content');
    ?>
</div>


<?php get_footer();