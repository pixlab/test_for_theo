<?php
/*
 *  CONTAINER SHORTCODE
 */

if ( !function_exists('container_shortcode') ) {

	function container_shortcode($atts, $content = null) {

		extract(shortcode_atts(
            array(
                'class'  => ''
            ), $atts));
        
        $class_arr = explode(' ', $class);

		$initial_class = ( in_array('fluid', $class_arr) ) ? 'container-fluid': 'container';
		$output = '<div class="'.$initial_class.' '.$class.'">'.do_shortcode($content).'</div>';

		return $output;
	}
	add_shortcode('container', 'container_shortcode');

}


/***************************************************/

/*
 *  COLUMN SHORTCODE
 */

if ( !function_exists('bs_column_column_shortcode') ) {

	function bs_column_column_shortcode($atts, $content = null) {

		extract(shortcode_atts(
            array(
                'class' => ''
            ), $atts));

		$output = '<div class="'.$class.'">'.do_shortcode($content).'</div>';
		return $output;
	}
	add_shortcode('bs_column', 'bs_column_column_shortcode');

}

/***************************************************/

/*
 *  COLUMN  inner SHORTCODE
 */

if ( !function_exists('column_inner_shortcode') ) {

	function column_inner_shortcode($atts, $content = null) {

		extract(shortcode_atts(
            array(
                'class' => ''
            ), $atts));

		$output = '<div class="'.$class.'">'.do_shortcode($content).'</div>';
		return $output;
	}
	add_shortcode('bs_column_inner', 'column_inner_shortcode');

}

/***************************************************/

/*
 *  ROW SHORTCODE
 */

if ( !function_exists('row_shortcode') ) {

	function row_shortcode($atts, $content = null) {
		extract(shortcode_atts(
            array(
                'class' => ''
            ), $atts));

		$output = '<div class="row '.$class.'">'.do_shortcode($content).'</div>';

		return $output;
	}
	add_shortcode('row', 'row_shortcode');

}

/***************************************************/

/*
 *  ROW inner SHORTCODE
 */

if ( !function_exists('row_inner_shortcode') ) {

	function row_inner_shortcode($atts, $content = null) {
		extract(shortcode_atts(
            array(
                'class' => ''
            ), $atts));

		$output = '<div class="row '.$class.'">'.do_shortcode($content).'</div>';

		return $output;
	}
	add_shortcode('row_inner', 'row_inner_shortcode');

}

/***************************************************/

/*
 *  BR SHORTCODE
 */

if ( !function_exists('br_shortcode') ) {

	function br_shortcode($atts, $content = null) {

		$output = '<br class="custom-br">';

		return $output;
	}
	add_shortcode('br', 'br_shortcode');

}

/***************************************************/

/*
 *  CURRENT YEAR SHORTCODE
 */

if ( !function_exists('current_year_shortcode') ) {

	function current_year_shortcode() {	

		$output = '<span>'.date("Y").'</span>';
		return $output; 
	} 
	add_shortcode('current_year', 'current_year_shortcode');

}


/*
 *  ONLY FOR MOBILE SHORTCODE
 */

if ( !function_exists('only_for_mobile_shortcode') ) {

	function only_for_mobile_shortcode($atts, $content = null) {

		$detect2 = new Mobile_Detect;
		if ( $detect2->isMobile() ) { 
			$output = '<section class="only_for_mobile">'.do_shortcode($content).'</section>';
		}
		else $output = '';	

		return $output; 
	} 
	add_shortcode('only_for_mobile', 'only_for_mobile_shortcode');

}


/*
 *  ONLY FOR DESKTOP SHORTCODE
 */

if ( !function_exists('only_for_desktop_shortcode') ) {

	function only_for_desktop_shortcode($atts, $content = null) {

		$detect2 = new Mobile_Detect;
		if ( !$detect2->isMobile() ) { 
			$output = '<section class="only_for_desktop">'.do_shortcode($content).'</section>';
		}
		else $output = '';	

		return $output; 
	} 
	add_shortcode('only_for_desktop', 'only_for_desktop_shortcode');

}



/*
 *  admin_notes - you should use this shortcode
 *  if you want add big comment in admin editor, but you don't
 *  want to display this info in front area
 */
 
if ( !function_exists('admin_notes_shortcode') ) {

	function admin_notes_shortcode($atts, $content = null) {
		return ''; 
	} 
	add_shortcode('admin_notes', 'admin_notes_shortcode');

}

/**************************/

/*
 * Content for logged user shortcode
 */

if ( !function_exists('logged_user_shortcode') ) {

	function logged_user_shortcode($atts, $content = null) {

	    if ( is_user_logged_in() ) {
		    return do_shortcode( $content );
        }

        return '';
	}
	add_shortcode('logged_user', 'logged_user_shortcode');
}

/************************************************************************/

/*
 * hero_box shortcode
 */

if ( !function_exists('hero_box_shortcode') ) {
    function hero_box_shortcode($atts, $content = null) {
        extract(shortcode_atts(array(
            'class' => '',
        ), $atts));
        
        ob_start();
        
        $hero_box      = get_field('hero_box', get_queried_object_id());
        
        $hero_image    = ( is_array($hero_box) && isset($hero_box['image']) )
                                ? $hero_box['image']['url'] : null;
        
        $hero_caption  = ( is_array($hero_box) && isset($hero_box['caption']) )
                                ? $hero_box['caption'] : null;
        
        $hero_btn_text = ( is_array($hero_box) && isset($hero_box['button_text']) )
                                ? $hero_box['button_text'] : null;
        
        $hero_btn_url  = ( is_array($hero_box) && isset($hero_box['button_url']) )
                                ? $hero_box['button_url'] : null;
        
        $down_btn_text = ( is_array($hero_box) && isset($hero_box['scroll_down_btn']) )
                                ? $hero_box['scroll_down_btn'] : null;
        
        
        if ( !empty($hero_box) ) { ?>
            <div class="hero-box js-hero-box">
                <div class="container">
                <div class="row">
                <div class="col-xs-12-12 col-md-12-12 inner">
                    <?php
                    echo ( !is_null($hero_image) )
                            ? '<img src="'.$hero_image.'" alt="thumb" />' : '';
                    ?>
                    <div class="wrap-content">
                        <?php
                        echo ( !is_null($hero_caption) ) ? '<h1>'.do_shortcode($hero_caption).'</h1>' : '';
                        echo ( !is_null($hero_btn_text) && !is_null($hero_btn_url)  )
                                ? '<a href="'.$hero_btn_url.'" class="btn-primary">'.$hero_btn_text.'</a>'
                                : '';
                        ?>
        
                        <?php if ( !is_null($down_btn_text) ) { ?>
                            <button class="scroll-to-next-section js-scroll-to-next-section">
                                <span><?php echo $down_btn_text; ?></span>
                            </button>
                        <?php } ?>
                    </div>
                </div>
                </div>
                </div>
            </div>
        <?php }
        
        return ob_get_clean();
    }
    add_shortcode('hero_box', 'hero_box_shortcode');
}

/************************************************************************/

/*
 * vehicles_overview_with_filters shortcode
 */

if ( !function_exists('vehicles_overview_with_filters') ) {
    function vehicles_overview_with_filters($atts, $content = null) {
        extract(shortcode_atts(array(
            'class'      => '',
            'order'      => 'ASC',
            'sort_by'    => '',
            'show_posts' => '6',
            'load_new_posts' => '6',
            'box_caption' => '',
        ), $atts));
        
        ob_start();
    
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'vehicle' ),
            'post_status'            => array( 'publish' ),
            'posts_per_page'         => $show_posts,
            'posts_per_archive_page' => $show_posts,
            'order'                  => $order,
            'orderby'                => 'meta_value',
            'meta_key'               => $sort_by,
        );

        // The Query
        $query = new WP_Query( $args );

        // The Loop
        if ( $query->have_posts() ) {
            ?>
            <div class="vehicles-overview-box js-vehicles-overview-box">
                <div class="container">
                <div class="row">
                <div class="col-xs-12-12">
            
                <?php
                echo ( !empty($box_caption) )
                        ? '<h2>'.htmlspecialchars_decode($box_caption,ENT_NOQUOTES).'</h2>'
                        : '';
                ?>
                
                <form class="filter-vehicles-form js-filter-vehicles-form" method="post">
                    <input type="hidden" name="order" value="<?php echo $order; ?>">
                    <input type="hidden" name="posts_per_page" value="<?php echo $show_posts; ?>">
                    <input type="hidden" name="sort_by"    value="<?php echo $sort_by; ?>">
<!--                    <input type="hidden" name="new_posts" value="--><?php //echo $load_new_posts; ?><!--">-->
                    
                    <select class="input-element" name="terms-manufacturer">
                        <option value="" selected disabled>Select Manufacturer</option>
                        <?php
                        $terms_manufacturer = get_terms( [
                            'taxonomy' => 'manufacturer',
                            'hide_empty' => false,
                        ] );
                        
                        if ( !empty($terms_manufacturer) ) {
                            foreach ( $terms_manufacturer as $item ) {
                                echo '<option value="'.$item->term_id.'">'.$item->name.'</option>';
                            }
                        }
                        ?>
                    </select>
                    
                    <select class="input-element" name="terms-class">
                        <option value="" selected disabled>Select Class</option>
                        <?php
                        $terms_class = get_terms( [
                            'taxonomy' => 'class',
                            'hide_empty' => false,
                        ] );
                        
                        if ( !empty($terms_class) ) {
                            foreach ( $terms_class as $item ) {
                                echo '<option value="'.$item->term_id.'">'.$item->name.'</option>';
                            }
                        }
                        ?>
                    </select>

                    <input type="submit" class="input-element" value="<?php echo 'Filter'; ?>" />
                </form>
                
                
                <div class="inner js-inner">
                    <?php
                    while ( $query->have_posts() ) {
                        $query->the_post();
            
                        $post_ID = get_the_ID();
                        
                        $attach  = wp_get_attachment_image_src(
                                        get_post_thumbnail_id($post_ID),
                                        array(350, 186)
                                   );
                        $img_url = $attach['0'];
                        
                        $post_title = get_the_title();
                        $year       = get_field('vehicle_year');
                        $price      = get_field('vehicle_price');
            
                        $manufacturer_list = wp_get_post_terms( $post_ID, 'manufacturer' );
                        $class_list        = wp_get_post_terms( $post_ID, 'class' );
            
                        include( locate_template(
                                'template/loop-vehicle-item.php',
                                false,
                                false )
                               );
                    }
                    ?>
                </div>
                
                <div class="wrap-load-more">
                    <button class="btn-secondary js-load-more-posts"
                            data-manufacturer=""
                            data-class=""
                            data-offset="<?php echo $show_posts; ?>"
                            data-posts_per_page="<?php echo $show_posts; ?>"
                            data-load_new_posts="<?php echo $load_new_posts; ?>"
                            data-order="<?php echo $order; ?>"
                            data-sortby="<?php echo $sort_by; ?>"
                            ><?php _e("Load More"); ?></button>
                </div>
                
            </div></div></div></div>
            
            <?php
        } else {
            // no posts found
        }

        // Restore original Post Data
        wp_reset_postdata();
        
        
        return ob_get_clean();
    }
    add_shortcode('vehicles_overview_with_filters', 'vehicles_overview_with_filters');
}
