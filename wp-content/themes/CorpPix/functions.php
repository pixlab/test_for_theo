<?php
// include main Theme Class
require get_template_directory() . '/inc/Corppix.php';
$corppix = new Corppix();

/**
 * Main theme init
 */
add_action( 'after_setup_theme', array($corppix,'px_site_setup') );
add_filter( 'sanitize_file_name', array($corppix, 'custom_sanitize_file_name'), 10, 1 );

/**
 * Set custom upload size limit
 */
$corppix->px_custom_upload_size_limit(40);


/**
 *  Add custom setting
 */
add_action( 'after_setup_theme', function(){
    
    /**
     * Add custom logo theme support
     */
    $defaults = array(
        'height'      => 100,
        'width'       => 400,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
    
    
    /**
     * Enable upload svg images
     * @param $mimes
     *
     * @return mixed
     */
    function cc_mime_types($mimes) {
        $mimes['svg'] = 'image/svg+xml';
        return $mimes;
    }
    add_filter('upload_mimes', 'cc_mime_types');
    
    
    /**
     * Adding custom theme option page
     */
    if( function_exists('acf_add_options_page') ) {
        acf_add_options_page(array(
            'page_title' 	=> 'Theme General Settings',
            'menu_title'	=> 'Theme Settings',
            'menu_slug' 	=> 'theme-general-settings',
            'capability'	=> 'edit_posts',
            'redirect'		=> false
        ));
    }
    
});



/**
 * Enqueue scripts and styles
 */
function px_site_scripts() {

    // Load our main stylesheet.
    wp_enqueue_style( 'corppix_site-style', get_stylesheet_uri() );

    wp_enqueue_style('assistant_font', 'https://fonts.googleapis.com/css?family=Assistant:400,700&display=swap');
    wp_enqueue_style('libre_font', 'https://fonts.googleapis.com/css?family=Libre+Baskerville:400,700&display=swap');
    
    wp_enqueue_style('corppix_site_style', get_template_directory_uri().'/build/styles/screen.css');

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

    wp_localize_script( 'corppix_site-script', 'screenReaderText', array(
        'expand'   => '<span class="screen-reader-text">' . __( 'expand child menu', 'corppix_site' ) . '</span>',
        'collapse' => '<span class="screen-reader-text">' . __( 'collapse child menu', 'corppix_site' ) . '</span>',
    ) );

    wp_enqueue_script( 'libs_js', get_template_directory_uri() . '/build/js/libs.js', array('jquery'), null, true );

    wp_enqueue_script( 'customization_js', get_template_directory_uri() . '/build/js/customization.js', array('jquery', 'libs_js'), null, true );

    $vars = array(
        'ajax_url'   => admin_url( 'admin-ajax.php' ),
        'theme_path' => get_stylesheet_directory_uri(),
        'site_url'   => get_site_url()
    );

    wp_localize_script( 'customization_js', 'var_from_php', $vars );


    remove_action('wp_head', 'wp_print_scripts');
    remove_action('wp_head', 'wp_print_head_scripts', 9);
    remove_action('wp_head', 'wp_enqueue_scripts', 1);

    add_action('wp_footer', 'wp_print_scripts', 5);
    add_action('wp_footer', 'wp_enqueue_scripts', 5);
    add_action('wp_footer', 'wp_print_head_scripts', 5);

}
add_action( 'wp_enqueue_scripts', 'px_site_scripts' );



/**
 * Registers a new post type - vehicle
 * @uses $wp_post_types Inserts new post type object into the list
 *
 * @param string  Post type key, must not exceed 20 characters
 * @param array|string  See optional args description above.
 * @return object|WP_Error the registered post type object, or an error object
 */
function vehicle_register_name() {
    
    $labels = array(
        'name'               => __( 'Vehicles', 'corppix_site' ),
        'singular_name'      => __( 'Vehicle', 'corppix_site' ),
        'add_new'            => _x( 'Add New Vehicle', 'corppix_site', 'corppix_site' ),
        'add_new_item'       => __( 'Add New Vehicle', 'corppix_site' ),
        'edit_item'          => __( 'Edit Vehicle', 'corppix_site' ),
        'new_item'           => __( 'New Vehicle', 'corppix_site' ),
        'view_item'          => __( 'View Vehicle', 'corppix_site' ),
        'search_items'       => __( 'Search Vehicles', 'corppix_site' ),
        'not_found'          => __( 'No Vehicles found', 'corppix_site' ),
        'not_found_in_trash' => __( 'No Vehicles found in Trash', 'corppix_site' ),
        'parent_item_colon'  => __( 'Parent Vehicle:', 'corppix_site' ),
        'menu_name'          => __( 'Vehicles', 'corppix_site' ),
    );
    
    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'description'         => 'description',
        'taxonomies'          => array(),
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => null,
        'menu_icon'           => null,
        'show_in_nav_menus'   => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive'         => true,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => true,
        'capability_type'     => 'post',
        'supports'            => array(
            'title',
            'editor',
            'author',
            'thumbnail',
            'excerpt',
            'custom-fields',
            'trackbacks',
            'comments',
            'revisions',
            'page-attributes',
            'post-formats',
        ),
    );
    
    register_post_type( 'vehicle', $args );
}

add_action( 'init', 'vehicle_register_name' );


//create a custom taxonomy for Cases
add_action( 'init', 'vehicle_taxonomies', 0 );
function vehicle_taxonomies() {
    register_taxonomy('manufacturer', array('vehicle'), array(
        'hierarchical' => true,
        'labels'                => [
            'name'              => 'Manufacturer categories',
            'singular_name'     => 'Manufacturer category',
            'search_items'      => 'Search Manufacturer categories',
            'all_items'         => 'All Manufacturer category',
            'view_item '        => 'View Manufacturer category',
            'parent_item'       => 'Parent Manufacturer category',
            'parent_item_colon' => 'Parent Manufacturer category:',
            'edit_item'         => 'Edit Manufacturer category',
            'update_item'       => 'Update Manufacturer category',
            'add_new_item'      => 'Add New Manufacturer category',
            'new_item_name'     => 'New Manufacturer category Name',
            'menu_name'         => 'Manufacturer category',
        ],
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'manufacturer' ),
    ));
    
    
    register_taxonomy('class', array('vehicle'), array(
        'hierarchical' => true,
        'labels'                => [
            'name'              => 'Class categories',
            'singular_name'     => 'Class category',
            'search_items'      => 'Search Class categories',
            'all_items'         => 'All Class category',
            'view_item '        => 'View Class category',
            'parent_item'       => 'Parent Class category',
            'parent_item_colon' => 'Parent Class category:',
            'edit_item'         => 'Edit Class category',
            'update_item'       => 'Update Class category',
            'add_new_item'      => 'Add New Class category',
            'new_item_name'     => 'New Class category Name',
            'menu_name'         => 'Class category',
        ],
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'class' ),
    ));
}




/**
 *   AJAX request for getting new portion of posts
 */
function get_more_posts() {
    
    if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) { // check if it's ajax request (simple defence)
        
        // prepare string to be stored to database
        $posts_per_page       = filter_input(INPUT_POST, 'posts_per_page', FILTER_SANITIZE_NUMBER_INT);
        $offset       = filter_input(INPUT_POST, 'offset', FILTER_SANITIZE_NUMBER_INT);
        $manufacturer = filter_input(INPUT_POST, 'terms-manufacturer', FILTER_SANITIZE_NUMBER_INT);
        $terms_class  = filter_input(INPUT_POST, 'terms-class', FILTER_SANITIZE_NUMBER_INT);
        $order        = filter_input(INPUT_POST, 'order', FILTER_SANITIZE_STRING);
        $new_posts    = filter_input(INPUT_POST, 'new_posts', FILTER_SANITIZE_NUMBER_INT);
        $sort_by      = filter_input(INPUT_POST, 'sort_by', FILTER_SANITIZE_STRING);
    
        $offset = ( is_null($offset) ) ? 0 : $offset;
        
        $posts_per_page = ( !empty($new_posts) && !is_null($new_posts) ) ? $new_posts : $posts_per_page;
    
        /*var_dump($new_posts);
        var_dump($posts_per_page);
        var_dump($order);
        var_dump($manufacturer);
        var_dump($terms_class);
        var_dump($sort_by);
        var_dump($offset);*/
        
        
        
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'vehicle' ),
            'post_status'            => array( 'publish' ),
            'posts_per_page'         => $posts_per_page,
            'posts_per_archive_page' => $posts_per_page,
            'order'                  => $order,
            'orderby'                => 'meta_value',
            'meta_key'               => $sort_by,
            'offset'                 => $offset,
        );
    
        $manufacturer_query = '';
        $terms_class_query  = '';
    
        if ( !is_null($manufacturer) && !empty($manufacturer) ) {
            $manufacturer_query = array(
                'taxonomy' => 'manufacturer',
                'field'    => 'id',
                'terms'    => $manufacturer,
                'operator' => 'IN',
            );
        }
    
        if ( !is_null($terms_class) && !empty($terms_class) ) {
            $terms_class_query = array(
                'taxonomy' => 'class',
                'field'    => 'id',
                'terms'    => $terms_class,
                'operator' => 'IN',
            );
        }
    
        $args['tax_query'] = array(
            'relation' => 'AND',
            array($terms_class_query, $manufacturer_query)
        );
        
        // The Query
        $query = new WP_Query( $args );
    
        ob_start();
        
        // The Loop
        if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {
                $query->the_post();
            
                $post_ID = get_the_ID();
            
                $attach  = wp_get_attachment_image_src( get_post_thumbnail_id($post_ID), array(350, 186) );
                $img_url = $attach['0'];
            
                $post_title = get_the_title();
                $year       = get_field('vehicle_year');
                $price      = get_field('vehicle_price');
            
                $manufacturer_list = wp_get_post_terms( $post_ID, 'manufacturer' );
                $class_list        = wp_get_post_terms( $post_ID, 'class' );
            
                include( locate_template( 'template/loop-vehicle-item.php', false, false ) );
            
            }
    
            wp_send_json_success( array(true, ob_get_clean()) );
        } else {
            wp_send_json_success( array(false, '<p class="text-center end-of-list">'.__("No more posts").'</p>') );
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        wp_send_json_error('<p class="text-center end-of-list">Something goes wrong!</p>');
        
    }
    
    die;
    
}
$corppix->ajax_front_to_backend('get_more_posts','get_more_posts');




/**
 *   AJAX request for getting filtered posts
 */
/*function filter_vehicles() {
    
    if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) { // check if it's ajax request (simple defence)
        
        $manufacturer = filter_input(INPUT_POST, 'terms-manufacturer', FILTER_SANITIZE_STRING);
        $terms_class  = filter_input(INPUT_POST, 'terms-class', FILTER_SANITIZE_STRING);
        $order        = filter_input(INPUT_POST, 'order', FILTER_SANITIZE_STRING);
        $sort_by      = filter_input(INPUT_POST, 'sort_by', FILTER_SANITIZE_STRING);
    
        $manufacturer_query = '';
        $terms_class_query  = '';
    
        if ( !is_null($manufacturer) && !empty($manufacturer) ) {
            $manufacturer_query = array(
                'taxonomy' => 'manufacturer',
                'field'    => 'id',
                'terms'    => $manufacturer,
                'operator' => 'IN',
            );
        }
    
        if ( !is_null($terms_class) && !empty($terms_class) ) {
            $terms_class_query = array(
                'taxonomy' => 'class',
                'field'    => 'id',
                'terms'    => $terms_class,
                'operator' => 'IN',
            );
        }
        
    
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'vehicle' ),
            'post_status'            => array( 'publish' ),
            'posts_per_page'         => -1,
            'posts_per_archive_page' => -1,
            'order'                  => $order,
            'orderby'                => 'meta_value',
            'meta_key'               => $sort_by,
        );
    
        $args['tax_query'] = array(
            'relation' => 'AND',
            array($terms_class_query, $manufacturer_query)
        );
        
    
        // The Query
        $query = new WP_Query( $args );
    
        ob_start();
        
        // The Loop
        if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {
                $query->the_post();
            
                $post_ID = get_the_ID();
            
                $attach  = wp_get_attachment_image_src( get_post_thumbnail_id($post_ID), array(350, 186) );
                $img_url = $attach['0'];
            
                $post_title = get_the_title();
                $year       = get_field('vehicle_year');
                $price      = get_field('vehicle_price');
            
                $manufacturer_list = wp_get_post_terms( $post_ID, 'manufacturer' );
                $class_list        = wp_get_post_terms( $post_ID, 'class' );
            
                include( locate_template( 'template/loop-vehicle-item.php', false, false ) );
            
            }
    
            wp_send_json_success( array(true, ob_get_clean()) );
        } else {
            wp_send_json_success( array(false, '<p class="text-center end-of-list">'.__("No posts found").'</p>') );
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        wp_send_json_error('<p class="text-center end-of-list">Something goes wrong!</p>');
        
    }
    
    die;
}
$corppix->ajax_front_to_backend('filter_vehicles','filter_vehicles');
*/




