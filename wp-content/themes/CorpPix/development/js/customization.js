;
import smoothscroll from 'smoothscroll-polyfill';
import {closest_polyfill, scroll_to_next_section} from './modules/helpers.js';

(function($){

	document.addEventListener("DOMContentLoaded", () => {

		const BODY                       = document.querySelector('body');
		const LOAD_MORE_POSTS_BTN        = document.querySelector('.js-load-more-posts');
		const SCROLL_TO_NEXT_SECTION_BTN = document.querySelector('.js-scroll-to-next-section');
		const VEHICLES_OVERVIEW_INNER    = document.querySelector('.js-vehicles-overview-box .js-inner');
		const FILTER_VEHICLES_FORM       = document.querySelector('.js-filter-vehicles-form');

		window.onload = function(){

			BODY.classList.add('loaded');

			// kick off the polyfill!
			smoothscroll.polyfill();

			// Init Closest polyfill method
			closest_polyfill();


			/**
			 * Scroll to next section
			 */
			(SCROLL_TO_NEXT_SECTION_BTN)
			&& scroll_to_next_section(
				SCROLL_TO_NEXT_SECTION_BTN,
				SCROLL_TO_NEXT_SECTION_BTN.closest('.js-hero-box')
			);


			/**
			 * load new posts
			 */
			(LOAD_MORE_POSTS_BTN)
			&& LOAD_MORE_POSTS_BTN.addEventListener('click', event => {

				event.preventDefault();

				const OFFSET         = +event.target.dataset.offset;
				const LOAD_NEW_POSTS = +event.target.dataset.load_new_posts;
				const SORT_BY        = event.target.dataset.sortby;
				const POSTS_PER_PAGE = +event.target.dataset.posts_per_page;
				const MANUFACTURER   = +event.target.dataset.manufacturer;
				const TERM_CLASS     = +event.target.dataset.class;

				$.ajax({
					url: var_from_php.ajax_url,
					type: 'post',
					data: {
						'offset'       : OFFSET,
						'terms-manufacturer' : MANUFACTURER,
						'terms-class'        : TERM_CLASS,
						'order'        : event.target.dataset.order,
						'sort_by'      : SORT_BY,
						'posts_per_page' : POSTS_PER_PAGE,
						'new_posts'    : LOAD_NEW_POSTS,
						'action'       : 'get_more_posts',
					},
					success: function (response) {
						if (VEHICLES_OVERVIEW_INNER) {
							VEHICLES_OVERVIEW_INNER.innerHTML += response.data[1];

							event.target.dataset.offset = OFFSET + LOAD_NEW_POSTS;

							if ( !response.data[0] ) {
								event.target.style.display = 'none';
							}
						}
					}
				});

			});


			/**
			 *  Handler for submit filter form
			 */
			(FILTER_VEHICLES_FORM)
			&& FILTER_VEHICLES_FORM.addEventListener('submit', event=> {

				event.preventDefault();

				const TERM_MANUFACTURER = FILTER_VEHICLES_FORM.querySelector('select[name="terms-manufacturer"]').value;

				const TERM_CLASS = FILTER_VEHICLES_FORM.querySelector('select[name="terms-class"]').value;
				const END_OF_LIST_ELEMENT = event.target.querySelector('.end-of-list');

				// Update search parameters
				LOAD_MORE_POSTS_BTN.dataset.manufacturer = TERM_MANUFACTURER;
				LOAD_MORE_POSTS_BTN.dataset.class        = TERM_CLASS;
				LOAD_MORE_POSTS_BTN.dataset.offset       = LOAD_MORE_POSTS_BTN.dataset.posts_per_page;
				LOAD_MORE_POSTS_BTN.style.display        = 'inline-flex';

				(END_OF_LIST_ELEMENT) && END_OF_LIST_ELEMENT.remove();

				let formData = new FormData( event.target );
				formData.append('action', 'get_more_posts');

				// making ajax request to backend to get more events for current date of month
				$.ajax({
					url : var_from_php.ajax_url,
					type: 'post',
					data: formData,
					//dataType: 'json',
					processData: false,
					contentType: false,
					success: function (response) {
						if (VEHICLES_OVERVIEW_INNER) {
							VEHICLES_OVERVIEW_INNER.innerHTML = response.data[1];
						}
					}
				});

			});

		};  // end of onload event

		window.onresize = function(){

			
		};

	});  // end of ready event

})(jQuery);
