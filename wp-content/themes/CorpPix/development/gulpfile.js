const gulp = require('gulp');

const { src, dest, series, parallel } = require('gulp');
const sass         = require('gulp-sass');
const sourcemaps   = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const browserify   = require('browserify');
const eslint       = require('gulp-eslint');
const uglify       = require('gulp-uglify');
const concat       = require('gulp-concat');
const gutil        = require('gulp-util');
const plumber      = require('gulp-plumber');
const source       = require('vinyl-source-stream');
const buffer       = require('vinyl-buffer');


sass.compiler = require('node-sass');

const paths = {
    styles: {
        src: 'sass/**/*.scss',
        dest: '../build/styles/'
    },
    scripts: {
        src: 'js/**/*.js',
        dest: '../build/js/'
    }
};


function js_compile(){
    // based on https://github.com/gulpjs/gulp/blob/master/docs/recipes/browserify-uglify-sourcemap.md
    let b = browserify({
        entries: 'js/customization.js',
        debug: true
    }).transform("babelify", {
        presets: ["@babel/env"]
    });

    return b.bundle()
        .pipe(source('customization.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({ loadMaps: true }))
		.pipe(plumber())
		.pipe(uglify())
        .pipe(sourcemaps.write('./'))
		.pipe(gulp.dest(paths.scripts.dest))
		.on('error', gutil.log);
}

function js_lint(){
    return gulp.src('js/customization.js')
        .pipe( eslint() )
        .pipe( eslint.format() )
        // .pipe( eslint.failAfterError() )
        .on('error', gutil.log);
}

function combile_libs_js(){
	return gulp.src([
			'js/js.cookie.js',
		])
		.pipe(concat('libs.js'))
		.pipe(gulp.dest(paths.scripts.dest))
		.on('error', gutil.log);
}

function styles() {
    return gulp.src(paths.styles.src)
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
	    .pipe(autoprefixer({
		    overrideBrowserslist: "last 3 version"
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(paths.styles.dest));
}

const js = gulp.parallel(combile_libs_js, js_compile, js_lint);

function watch() {
    gulp.watch(paths.styles.src, styles);
    gulp.watch(paths.scripts.src, js);
}

/*
 * You can use CommonJS `exports` module notation to declare tasks
 */
exports.watch = watch;
exports.build = gulp.parallel(styles, combile_libs_js, js_compile, js_lint);
